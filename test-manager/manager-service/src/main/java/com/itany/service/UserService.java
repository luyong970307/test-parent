package com.itany.service;

import com.github.pagehelper.PageInfo;
import com.itany.pojo.User;

public interface UserService {
    public PageInfo<User> findUserAll(Integer page, Integer rows);
}
