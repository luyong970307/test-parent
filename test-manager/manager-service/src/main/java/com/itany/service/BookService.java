package com.itany.service;

import com.github.pagehelper.PageInfo;
import com.itany.pojo.Book;

public interface BookService {
    public PageInfo<Book> findAll(Integer page, Integer rows);
    public Book findById (String id);
    public void updateFlagById(Book book);

}
