package com.itany.service;

import com.github.pagehelper.PageInfo;
import com.itany.pojo.Book;
import com.itany.pojo.Supplier;

import java.util.List;

public interface SupplierService {
    public List<Supplier> findAll();

}
