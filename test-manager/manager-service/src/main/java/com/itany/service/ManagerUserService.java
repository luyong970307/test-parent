package com.itany.service;


import com.github.pagehelper.PageInfo;
import com.itany.exception.UserNotExistsException;
import com.itany.pojo.ManagerUser;
import com.itany.pojo.User;

public interface ManagerUserService {

	public ManagerUser findByUsernameAndPassword(String username, String password) throws UserNotExistsException;
	
}
