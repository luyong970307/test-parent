package com.itany.book.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itany.mapper.ManagerUserMapper;
import com.itany.mapper.UserMapper;
import com.itany.pojo.ManagerUser;
import com.itany.pojo.User;
import com.itany.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Transactional(readOnly=true)
    public PageInfo<User> findUserAll(Integer page, Integer rows) {
        PageHelper.startPage(page, rows);
		List<User> list = userMapper.findUserAll();
		PageInfo<User> info = new PageInfo<User>(list);
		return info;
    }
}
