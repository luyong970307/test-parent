package com.itany.book.service.impl;

import com.itany.mapper.SupplierMapper;
import com.itany.pojo.Supplier;
import com.itany.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(readOnly = true)
public class SupplierServiceImpl implements SupplierService {
    @Autowired
    private SupplierMapper supplierMapper;


    public List<Supplier> findAll() {
        List<Supplier> suppliers = supplierMapper.findAll();
        return suppliers;
    }
}
