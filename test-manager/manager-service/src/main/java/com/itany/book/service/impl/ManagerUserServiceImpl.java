package com.itany.book.service.impl;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itany.exception.UserNotExistsException;
import com.itany.mapper.ManagerUserMapper;
import com.itany.mapper.UserMapper;
import com.itany.pojo.ManagerUser;
import com.itany.pojo.User;
import com.itany.service.ManagerUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional(rollbackFor=Exception.class,propagation= Propagation.REQUIRED)
public class ManagerUserServiceImpl implements ManagerUserService {

	@Autowired
	private ManagerUserMapper managerUserMapper;



	@Transactional(readOnly=true)
	public ManagerUser findByUsernameAndPassword(String username, String password) throws UserNotExistsException {
		ManagerUser mu = new ManagerUser();
		mu.setUsername(username);
		mu.setPassword(password);
		ManagerUser managerUser = managerUserMapper.login(mu);
		if (managerUser == null){
			throw new UserNotExistsException("用户名或密码错误");
		}
		return managerUser;
	}


}
