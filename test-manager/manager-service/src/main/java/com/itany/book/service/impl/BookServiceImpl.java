package com.itany.book.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itany.constant.StatusConstant;
import com.itany.mapper.BookMapper;
import com.itany.pojo.Book;
import com.itany.pojo.Examine;
import com.itany.pojo.User;
import com.itany.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor=Exception.class,propagation= Propagation.REQUIRED)
public class BookServiceImpl implements BookService {
    @Autowired
    private BookMapper bookMapper;

    @Transactional(readOnly=true)
    public PageInfo<Book> findAll(Integer page, Integer rows) {
        PageHelper.startPage(page, rows);
        List<Book> list = bookMapper.findAll();
        PageInfo<Book> info = new PageInfo<Book>(list);
        return info;
    }

    @Transactional(readOnly = true)
    public Book findById(String id){
        Book book = bookMapper.findById(Integer.parseInt(id));
        return book;
    }

    @Transactional(readOnly = false)
    public void updateFlagById(Book book){
        bookMapper.updateFlagById(book);
    }






}
