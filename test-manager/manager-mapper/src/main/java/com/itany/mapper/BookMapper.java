package com.itany.mapper;

import com.itany.pojo.Book;

import java.util.List;

public interface BookMapper {
    public List<Book> findAll();

    public Book findById(Integer id);

    public void updateById(Book book);

    public void updateFlagById(Book book);
    
    public int insertBook(Book book);
}
