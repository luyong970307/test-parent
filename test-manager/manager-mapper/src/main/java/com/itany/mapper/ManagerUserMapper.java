package com.itany.mapper;

import com.itany.pojo.ManagerUser;

public interface ManagerUserMapper {
    public ManagerUser login(ManagerUser managerUser);
}
