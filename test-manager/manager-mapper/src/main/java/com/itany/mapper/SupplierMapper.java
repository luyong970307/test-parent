package com.itany.mapper;

import com.itany.pojo.Supplier;

import java.util.List;

public interface SupplierMapper {
  public List<Supplier> findAll();
}
