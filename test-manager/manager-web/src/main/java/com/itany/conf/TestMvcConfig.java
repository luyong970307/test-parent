package com.itany.conf;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class TestMvcConfig implements WebMvcConfigurer {

    //添加ViewController
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/showlist").setViewName("userlist");
        //登录页面
        registry.addViewController("/showLogin").setViewName("login");
        //后台主页
        registry.addViewController("/showIndex").setViewName("index");
        // 用户管理
        registry.addViewController("/showUserList").setViewName("userlist");
        // 订单管理
        registry.addViewController("/showOrderList").setViewName("orderlist");
        // 书籍管理
        registry.addViewController("/showBookList").setViewName("booklist");
        // 入库审核(供应商)
        registry.addViewController("/showSupplierExamine").setViewName("supplier_examine");
        // 入库审核(平台)
        registry.addViewController("/showSupplierPlatform").setViewName("supplier_platform");
        // 权限管理
        registry.addViewController("/showPermission").setViewName("permission");
        // 角色管理
        registry.addViewController("/showRole").setViewName("role");
        // 管理员管理
        registry.addViewController("/showManager").setViewName("manager");

    }
}
