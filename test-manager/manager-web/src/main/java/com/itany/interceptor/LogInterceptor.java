package com.itany.interceptor;

import com.itany.pojo.ManagerUser;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LogInterceptor implements HandlerInterceptor {
    /**
     * 在处理Handler业务方法之前执行
     * @param request
     * @param response
     * @param handler
     * @return 返回值用于判断是否放行
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        ManagerUser ms  = (ManagerUser) request.getSession().getAttribute("ms");
        if(ms == null){
          request.setAttribute("logMsg","请先登录");
          request.getRequestDispatcher("/showLogin").forward(request,response);
          return false;
        }

        return true;
    }


    /**
     * 在处理Handler业务方法之后执行
     * @param request
     * @param response
     * @param handler
     * @param modelAndView
     * @throws Exception
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    /**
     * 在返回响应,响应到达之前执行
     * @param request
     * @param response
     * @param handler
     * @param ex
     * @throws Exception
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
