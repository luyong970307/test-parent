package com.itany.controller;

import com.github.pagehelper.PageInfo;
import com.itany.pojo.Book;
import com.itany.service.BookService;
import com.itany.vo.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/book")
public class BookController {
    @Autowired
    private BookService bookService;

    @RequestMapping("/findAll")
	@ResponseBody
	public Map<String,Object> findAll(@RequestParam(defaultValue="1")Integer page,
			                          @RequestParam(defaultValue="10")Integer rows){

		Map<String,Object> map = new HashMap<String, Object>();
		PageInfo<Book> info = bookService.findAll(page, rows);
		map.put("total", info.getTotal());
		map.put("rows", info.getList());
		return map;
	}


    @RequestMapping("/showModify")
	@ResponseBody
	public AjaxResult showModify(HttpServletRequest request){
    	AjaxResult rs = new AjaxResult();
    	String id = request.getParameter("id");
    	Book book = bookService.findById(id);
    	rs.setSuccess(true);
   		rs.setData(book);
    	return rs;
	}


	@RequestMapping("/modifyFlag")
	@ResponseBody
	public AjaxResult modifyFlag(Book book){
    	AjaxResult rs = new AjaxResult();
		bookService.updateFlagById(book);
		rs.setSuccess(true);
		rs.setMsg("修改成功");
		return rs;
	}

	@RequestMapping("/modifyBook")
	@ResponseBody
	public AjaxResult modify(Book book){
		System.out.println("----"+book);
    	AjaxResult rs = new AjaxResult();
    	return rs;
	}



}
