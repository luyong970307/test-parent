package com.itany.controller;


import com.github.pagehelper.PageInfo;
import com.itany.exception.UserNotExistsException;
import com.itany.pojo.ManagerUser;
import com.itany.pojo.User;
import com.itany.service.ManagerUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/managerUser")
public class ManagerUserController {
	
	@Autowired
	ManagerUserService managerUserService;


	@RequestMapping("/login")
	public String login(HttpServletRequest request){
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		System.out.println(username);
		System.out.println(password);
		try {
			ManagerUser managerUser = managerUserService.findByUsernameAndPassword(username, password);
			System.out.println(managerUser);
			if (managerUser.getSupplierid() == null){
				return "index";
			} else {
				return "login";
			}
		} catch (UserNotExistsException e) {
			e.printStackTrace();
			request.setAttribute("logMsg",e.getMessage());
			return "login";
		}
	}



//	@RequestMapping("/findAll")
//	@ResponseBody
//	public Map<String,Object> findAll(@RequestParam(defaultValue="1")Integer page,
//			                          @RequestParam(defaultValue="10")Integer rows){
//
//		Map<String,Object> map = new HashMap<String, Object>();
//		PageInfo<User> info = testService.findUserAll(page, rows);
//		map.put("total", info.getTotal());
//		map.put("rows", info.getList());
//		return map;
//	}

}
