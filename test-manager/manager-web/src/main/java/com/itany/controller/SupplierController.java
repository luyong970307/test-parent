package com.itany.controller;

import com.github.pagehelper.PageInfo;
import com.itany.pojo.Book;
import com.itany.pojo.Supplier;
import com.itany.service.SupplierService;
import com.itany.vo.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/supplier")
public class SupplierController {
    @Autowired
    private SupplierService supplierService;

    @RequestMapping("/findAll")
    @ResponseBody
   public AjaxResult findAll(){
        AjaxResult rs = new AjaxResult();
        List<Supplier> suppliers = supplierService.findAll();
        rs.setData(suppliers);
        return rs;
    }
}
