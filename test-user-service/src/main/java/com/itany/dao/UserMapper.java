package com.itany.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by tyh on 2018/9/22.
 */
@Repository
public interface UserMapper {

    public List<User> findUserAll();

    public void addUser(User user);

    public User login(User user);
}
