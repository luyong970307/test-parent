package com.itany.user.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itany.dao.UserMapper;
import com.itany.user.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by tyh on 2018/9/22.
 */
@Service
@com.alibaba.dubbo.config.annotation.Service
public class UserService implements IUserService{

    @Autowired
    private UserMapper userMapper;

    @Transactional(propagation = Propagation.REQUIRED,rollbackFor = Exception.class)
    public void addUser(User user) {
        user.setCreateDate(new Date());
        userMapper.addUser(user);
        System.out.println(1/0);
    }


    @Override
    public PageInfo<User> findUsers(Integer pageNo, Integer pageSize) {
        PageHelper.startPage(pageNo,pageSize);
        List<User> list =  userMapper.findUserAll();
        PageInfo<User> pageInfo = new PageInfo<User>(list);
        return pageInfo;
    }

    @Override
    public User login(User user) {
        return userMapper.login(user);
    }
}
