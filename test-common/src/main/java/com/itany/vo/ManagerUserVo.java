package com.itany.vo;

import com.itany.pojo.Permission;
import com.itany.pojo.Supplier;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class ManagerUserVo implements Serializable {
    private Integer id;
    private String username;
    private String password;
    private Integer supplierid;
    private Supplier supplier;
    private List<Permission> permissions;

    public ManagerUserVo() {
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ManagerUserVo that = (ManagerUserVo) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(username, that.username) &&
                Objects.equals(password, that.password) &&
                Objects.equals(supplierid, that.supplierid) &&
                Objects.equals(supplier, that.supplier) &&
                Objects.equals(permissions, that.permissions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, password, supplierid, supplier, permissions);
    }

    @Override
    public String toString() {
        return "ManagerUserVo{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", supplierid=" + supplierid +
                ", supplier=" + supplier +
                ", permissions=" + permissions +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getSupplierid() {
        return supplierid;
    }

    public void setSupplierid(Integer supplierid) {
        this.supplierid = supplierid;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }
}
