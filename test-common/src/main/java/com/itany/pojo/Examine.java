package com.itany.pojo;

import java.io.Serializable;


public class Examine implements Serializable {
    private Integer id;
    private Integer bookid;
    private Integer number;
    private String info;
    private Integer supplierid;
    private String title;
    private Integer flag;

    @Override
    public String toString() {
        return "Examine{" +
                "id=" + id +
                ", bookid=" + bookid +
                ", number=" + number +
                ", info='" + info + '\'' +
                ", supplierid=" + supplierid +
                ", title='" + title + '\'' +
                ", flag=" + flag +
                '}';
    }

    public Examine() {
    }

    public Examine(Integer id, Integer bookid, Integer number, String info, Integer supplierid, String title, Integer flag) {
        this.id = id;
        this.bookid = bookid;
        this.number = number;
        this.info = info;
        this.supplierid = supplierid;
        this.title = title;
        this.flag = flag;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBookid() {
        return bookid;
    }

    public void setBookid(Integer bookid) {
        this.bookid = bookid;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Integer getSupplierid() {
        return supplierid;
    }

    public void setSupplierid(Integer supplierid) {
        this.supplierid = supplierid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }
}
