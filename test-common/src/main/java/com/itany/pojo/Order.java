package com.itany.pojo;

import com.sun.jndi.cosnaming.IiopUrl;

import java.awt.print.Book;
import java.io.Serializable;

public class Order implements Serializable {
    private Integer id;
    private String no;
    private String alipayno;
    private User user;
    private Address address;
    private String info;
    private Double price;
    private Integer flage;

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", no='" + no + '\'' +
                ", alipayno='" + alipayno + '\'' +
                ", user=" + user +
                ", address=" + address +
                ", info='" + info + '\'' +
                ", price=" + price +
                ", flage=" + flage +
                '}';
    }

    public Order() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getAlipayno() {
        return alipayno;
    }

    public void setAlipayno(String alipayno) {
        this.alipayno = alipayno;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getFlage() {
        return flage;
    }

    public void setFlage(Integer flage) {
        this.flage = flage;
    }
}
