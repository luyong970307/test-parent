package com.itany.pojo;

import java.io.Serializable;

/**
 * Author:shengwancheng@itany.com
 * Date:19-10-22 下午6:20
 * descript:
 * vertsion:1.0
 */
public class Province implements Serializable {
    private Integer id;
    private String name;
    private Integer flag;
    private Integer type;

    @Override
    public String toString() {
        return "Province{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", flag=" + flag +
                ", type=" + type +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Province() {
    }

    public Province(Integer id, String name, Integer flag, Integer type) {
        this.id = id;
        this.name = name;
        this.flag = flag;
        this.type = type;
    }
}
