package com.itany.pojo;

import java.io.Serializable;

/**
 * Author:keqiong@itany.com
 * Date:19-10-22 下午6:06
 * descript:
 * veision:1.0
 */
public class Press implements Serializable {

    private Integer id;

    private String name;

    private Integer flag;

    public Press(Integer id, String name, Integer flag) {
        this.id = id;
        this.name = name;
        this.flag = flag;
    }

    public Press() {
    }

    @Override
    public String toString() {
        return "Press{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", flag=" + flag +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }
}
