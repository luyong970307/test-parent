package com.itany.pojo;

import java.io.Serializable;

/**
 * Author:shengwancheng@itany.com
 * Date:19-10-22 下午6:15
 * descript:
 * vertsion:1.0
 */
public class BookSupplier implements Serializable {
    private Integer id;
    private Integer bookid;
    private Integer supplierid;
    private Integer sellnum;
    private Integer number;

    @Override
    public String toString() {
        return "BookSupplier{" +
                "id=" + id +
                ", bookid=" + bookid +
                ", supplierid=" + supplierid +
                ", sellnum=" + sellnum +
                ", number=" + number +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBookid() {
        return bookid;
    }

    public void setBookid(Integer bookid) {
        this.bookid = bookid;
    }

    public Integer getSupplierid() {
        return supplierid;
    }

    public void setSupplierid(Integer supplierid) {
        this.supplierid = supplierid;
    }

    public Integer getSellnum() {
        return sellnum;
    }

    public void setSellnum(Integer sellnum) {
        this.sellnum = sellnum;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public BookSupplier() {
    }

    public BookSupplier(Integer id, Integer bookid, Integer supplierid, Integer sellnum, Integer number) {
        this.id = id;
        this.bookid = bookid;
        this.supplierid = supplierid;
        this.sellnum = sellnum;
        this.number = number;
    }
}
