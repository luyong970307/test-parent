package com.itany.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * Author:keqiong@itany.com
 * Date:19-10-22 下午6:05
 * descript:
 * veision:1.0
 */
public class Book implements Serializable {

    private Integer id;

    private String bookName;

    private String author;

    private Date publishDate;

    private Double groupPrice;

    private Double price;

    private String format;

    private String pagNumber;

    private String isbn;

    private String barcode;

    private Integer layout;

    private Integer printingNumber;

    private String register;

    private String weight;

    private String bookIntRoduce;

    private String authorIntRoduce;

    private String catalog;

    private Integer typeId;

    private Date createTime;

    private Integer pressId;

    private Integer number;

    private String imgurl;

    private Integer flag;

    private Integer recommend;

    public Book() {
    }

    public Book(Integer id, String bookName, String author, Date publishDate, Double groupPrice, Double price, String format, String pagNumber, String ISBN, String barcode, Integer layout, Integer printingNumber, String register, String weight, String bookIntRoduce, String authorIntRoduce, String catalog, Integer typeId, Date createTime, Integer pressId, Integer number, String imgurl, Integer flag, Integer recommend) {
        this.id = id;
        this.bookName = bookName;
        this.author = author;
        this.publishDate = publishDate;
        this.groupPrice = groupPrice;
        this.price = price;
        this.format = format;
        this.pagNumber = pagNumber;
        this.isbn = ISBN;
        this.barcode = barcode;
        this.layout = layout;
        this.printingNumber = printingNumber;
        this.register = register;
        this.weight = weight;
        this.bookIntRoduce = bookIntRoduce;
        this.authorIntRoduce = authorIntRoduce;
        this.catalog = catalog;
        this.typeId = typeId;
        this.createTime = createTime;
        this.pressId = pressId;
        this.number = number;
        this.imgurl = imgurl;
        this.flag = flag;
        this.recommend = recommend;
    }


    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", bookName='" + bookName + '\'' +
                ", author='" + author + '\'' +
                ", publishDate=" + publishDate +
                ", groupPrice=" + groupPrice +
                ", price=" + price +
                ", format='" + format + '\'' +
                ", pagNumber='" + pagNumber + '\'' +
                ", ISBN='" + isbn + '\'' +
                ", barcode='" + barcode + '\'' +
                ", layout=" + layout +
                ", printingNumber=" + printingNumber +
                ", register='" + register + '\'' +
                ", weight='" + weight + '\'' +
                ", bookIntRoduce='" + bookIntRoduce + '\'' +
                ", authorIntRoduce='" + authorIntRoduce + '\'' +
                ", catalog='" + catalog + '\'' +
                ", typeId=" + typeId +
                ", createTime=" + createTime +
                ", pressId=" + pressId +
                ", number=" + number +
                ", imgurl='" + imgurl + '\'' +
                ", flag=" + flag +
                ", recommend=" + recommend +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public Double getGroupPrice() {
        return groupPrice;
    }

    public void setGroupPrice(Double groupPrice) {
        this.groupPrice = groupPrice;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getPagNumber() {
        return pagNumber;
    }

    public void setPagNumber(String pagNumber) {
        this.pagNumber = pagNumber;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String ISBN) {
        this.isbn = isbn;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public Integer getLayout() {
        return layout;
    }

    public void setLayout(Integer layout) {
        this.layout = layout;
    }

    public Integer getPrintingNumber() {
        return printingNumber;
    }

    public void setPrintingNumber(Integer printingNumber) {
        this.printingNumber = printingNumber;
    }

    public String getRegister() {
        return register;
    }

    public void setRegister(String register) {
        this.register = register;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getBookIntRoduce() {
        return bookIntRoduce;
    }

    public void setBookIntRoduce(String bookIntRoduce) {
        this.bookIntRoduce = bookIntRoduce;
    }

    public String getAuthorIntRoduce() {
        return authorIntRoduce;
    }

    public void setAuthorIntRoduce(String authorIntRoduce) {
        this.authorIntRoduce = authorIntRoduce;
    }

    public String getCatalog() {
        return catalog;
    }

    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getPressId() {
        return pressId;
    }

    public void setPressId(Integer pressId) {
        this.pressId = pressId;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public Integer getRecommend() {
        return recommend;
    }

    public void setRecommend(Integer recommend) {
        this.recommend = recommend;
    }
}