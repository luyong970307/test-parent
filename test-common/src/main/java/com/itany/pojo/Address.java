package com.itany.pojo;

import java.io.Serializable;

public class Address implements Serializable {
    private Integer id;
    private String provincetext;
    private String citytext;
    private String regiontext;
    private String address;
    private User user;
    private String phone;

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", provincetext='" + provincetext + '\'' +
                ", citytext='" + citytext + '\'' +
                ", regiontext='" + regiontext + '\'' +
                ", address='" + address + '\'' +
                ", user=" + user +
                ", phone='" + phone + '\'' +
                '}';
    }

    public Address() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProvincetext() {
        return provincetext;
    }

    public void setProvincetext(String provincetext) {
        this.provincetext = provincetext;
    }

    public String getCitytext() {
        return citytext;
    }

    public void setCitytext(String citytext) {
        this.citytext = citytext;
    }

    public String getRegiontext() {
        return regiontext;
    }

    public void setRegiontext(String regiontext) {
        this.regiontext = regiontext;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
