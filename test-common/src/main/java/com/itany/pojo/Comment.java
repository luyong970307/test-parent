package com.itany.pojo;

import java.awt.print.Book;
import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {
    private Integer id;
    private String info;
    private Book book;
    private User user;
    private Integer level;
    private Date createtime;

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", info='" + info + '\'' +
                ", book=" + book +
                ", user=" + user +
                ", level=" + level +
                ", createtime=" + createtime +
                '}';
    }

    public Comment() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }
}
