package com.itany.pojo;


import java.awt.print.Book;
import java.io.Serializable;

public class Orderbook implements Serializable {
    private Integer id;
    private Book book;
    private Order order;
    private Integer num;
    private Double price;

    @Override
    public String toString() {
        return "Orderbook{" +
                "id=" + id +
                ", book=" + book +
                ", order=" + order +
                ", num=" + num +
                ", price=" + price +
                '}';
    }

    public Orderbook() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
