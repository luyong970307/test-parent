package com.itany.pojo;

import java.io.Serializable;

/**
 * Author:J.X.L@itany.com
 * Date:19-10-22 下午6:13
 * descript:
 * vertsion:1.0
 */
public class RolePermission implements Serializable {

    private Integer id;
    private Integer roleid;
    private Integer permissionid;

    public RolePermission() {
    }

    public RolePermission(Integer id, Integer roleid, Integer permissionid) {
        this.id = id;
        this.roleid = roleid;
        this.permissionid = permissionid;
    }

    @Override
    public String toString() {
        return "RolePermission{" +
                "id=" + id +
                ", roleid=" + roleid +
                ", permissionid=" + permissionid +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRoleid() {
        return roleid;
    }

    public void setRoleid(Integer roleid) {
        this.roleid = roleid;
    }

    public Integer getPermissionid() {
        return permissionid;
    }

    public void setPermissionid(Integer permissionid) {
        this.permissionid = permissionid;
    }
}
