package com.itany.pojo;

import java.io.Serializable;

/**
 * Author:shengwancheng@itany.com
 * Date:19-10-22 下午6:19
 * descript:
 * vertsion:1.0
 */
public class City implements Serializable {
    private Integer id;
    private String name;
    private Integer flag;
    private Integer countryid;
    private Integer type;

    @Override
    public String toString() {
        return "City{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", flag=" + flag +
                ", countryid=" + countryid +
                ", type=" + type +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public Integer getCountryid() {
        return countryid;
    }

    public void setCountryid(Integer countryid) {
        this.countryid = countryid;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public City() {
    }

    public City(Integer id, String name, Integer flag, Integer countryid, Integer type) {
        this.id = id;
        this.name = name;
        this.flag = flag;
        this.countryid = countryid;
        this.type = type;
    }
}
