package com.itany.pojo;

import java.io.Serializable;

/**
 * Author:J.X.L@itany.com
 * Date:19-10-22 下午6:10
 * descript:
 * vertsion:1.0
 */
public class UserRole implements Serializable {

    private Integer id;
    private Integer userid;
    private Integer roleid;

    public UserRole() {
    }

    public UserRole(Integer id, Integer userid, Integer roleid) {
        this.id = id;
        this.userid = userid;
        this.roleid = roleid;
    }

    @Override
    public String toString() {
        return "UserRole{" +
                "id=" + id +
                ", userid=" + userid +
                ", roleid=" + roleid +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getRoleid() {
        return roleid;
    }

    public void setRoleid(Integer roleid) {
        this.roleid = roleid;
    }
}
