package com.itany.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * Author:shengwancheng@itany.com
 * Date:19-10-22 下午6:12
 * descript:
 * vertsion:1.0
 */
public class Supplier implements Serializable {
    private Integer id;
    private String name;
    private Integer flag;
    private String info;
    private String linkman;
    private String phone;
    private Date createtime;

    @Override
    public String toString() {
        return "Supplier{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", flag=" + flag +
                ", info='" + info + '\'' +
                ", linkman='" + linkman + '\'' +
                ", phone='" + phone + '\'' +
                ", createtime=" + createtime +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getLinkman() {
        return linkman;
    }

    public void setLinkman(String linkman) {
        this.linkman = linkman;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Supplier() {
    }

    public Supplier(Integer id, String name, Integer flag, String info, String linkman, String phone, Date createtime) {
        this.id = id;
        this.name = name;
        this.flag = flag;
        this.info = info;
        this.linkman = linkman;
        this.phone = phone;
        this.createtime = createtime;
    }
}
