package com.itany.pojo;

import java.io.Serializable;

/**
 * Author:J.X.L@itany.com
 * Date:19-10-22 下午6:07
 * descript:
 * vertsion:1.0
 */
public class ManagerUser implements Serializable {

    private Integer id;
    private String username;
    private String password;
    private Integer supplierid;

    public ManagerUser() {
    }

    @Override
    public String toString() {
        return "ManagerUser{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", supplierid=" + supplierid +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getSupplierid() {
        return supplierid;
    }

    public void setSupplierid(Integer supplierid) {
        this.supplierid = supplierid;
    }
}
