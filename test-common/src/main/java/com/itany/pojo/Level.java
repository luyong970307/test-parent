package com.itany.pojo;

import java.io.Serializable;

/**
 * Author:J.X.L@itany.com
 * Date:19-10-22 下午6:16
 * descript:
 * vertsion:1.0
 */
public class Level implements Serializable {

    private Integer id;
    private String name;
    private Integer section;

    public Level() {
    }

    @Override
    public String toString() {
        return "Level{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", section=" + section +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSection() {
        return section;
    }

    public void setSection(Integer section) {
        this.section = section;
    }

    public Level(Integer id, String name, Integer section) {
        this.id = id;
        this.name = name;
        this.section = section;
    }
}
