package com.itany.pojo;

import java.io.Serializable;

/**
 * Author:shengwancheng@itany.com
 * Date:19-10-22 下午6:17
 * descript:
 * vertsion:1.0
 */
public class Region implements Serializable {
    private Integer id;
    private String name;
    private Integer flag;
    private Integer type;
    private Integer cityid;

    @Override
    public String toString() {
        return "Region{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", flag=" + flag +
                ", type=" + type +
                ", cityid=" + cityid +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getCityid() {
        return cityid;
    }

    public void setCityid(Integer cityid) {
        this.cityid = cityid;
    }

    public Region() {
    }

    public Region(Integer id, String name, Integer flag, Integer type, Integer cityid) {
        this.id = id;
        this.name = name;
        this.flag = flag;
        this.type = type;
        this.cityid = cityid;
    }
}
