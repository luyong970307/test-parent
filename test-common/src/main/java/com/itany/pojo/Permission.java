package com.itany.pojo;

import java.io.Serializable;

/**
 * Author:J.X.L@itany.com
 * Date:19-10-22 下午6:14
 * descript:
 * vertsion:1.0
 */
public class Permission implements Serializable {

    private Integer id;
    private String name;
    private Integer parentid;
    private String sname;
    private String url;

    @Override
    public String toString() {
        return "Permission{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", parentid=" + parentid +
                ", sname='" + sname + '\'' +
                ", url='" + url + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParentid() {
        return parentid;
    }

    public void setParentid(Integer parentid) {
        this.parentid = parentid;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Permission(Integer id, String name, Integer parentid, String sname, String url) {
        this.id = id;
        this.name = name;
        this.parentid = parentid;
        this.sname = sname;
        this.url = url;
    }

    public Permission() {
    }
}
