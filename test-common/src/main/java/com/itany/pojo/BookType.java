package com.itany.pojo;

import java.io.Serializable;

/**
 * Author:keqiong@itany.com
 * Date:19-10-22 下午6:21
 * descript:
 * veision:1.0
 */
public class BookType implements Serializable {

    private Integer id;

    private String typeText;

    private Integer pid;

    public BookType() {
    }

    public BookType(Integer id, String typeText, Integer pid) {
        this.id = id;
        this.typeText = typeText;
        this.pid = pid;
    }

    @Override
    public String toString() {
        return "BookType{" +
                "id=" + id +
                ", typeText='" + typeText + '\'' +
                ", pid=" + pid +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTypeText() {
        return typeText;
    }

    public void setTypeText(String typeText) {
        this.typeText = typeText;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }
}
