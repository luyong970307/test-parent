package com.itany.constant;

public interface StatusConstant {
    /**
     * 书籍状态:禁用
     */
    public static final Integer T_BOOK_FLAG_DISABLE = 0;
    /**
     * 书籍状态:启用
     */
    public static final Integer T_BOOK_FLAG_ENABLE = 1;
    /**
     * 书籍状态:(待审核)
     */
    public static final Integer T_BOOK_FLAG_WAITING = 2;
}
