package com.itany.conf;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class TestMvcConfig implements WebMvcConfigurer {



    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/showlogin").setViewName("login");
        registry.addViewController("/showregister").setViewName("register");
    }

}
