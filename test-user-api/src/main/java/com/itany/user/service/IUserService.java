package com.itany.user.service;

import com.github.pagehelper.PageInfo;

/**
 * Created by tyh on 2018/9/22.
 */
public interface IUserService {

    public void addUser(User user);

    public PageInfo<User> findUsers(Integer pageNo, Integer pageSize);

    public User login(User user);
}
